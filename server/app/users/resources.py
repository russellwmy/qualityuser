from datetime import datetime
from flask.ext.restful import request, Resource, fields
from app.users.models import User
from app import db
from app.utils import generate_code
from app.enums import *
from app.accounts.decorators import login_required, current_user

class UserInfoResource(Resource):

    @login_required
    def get(self,user_id):

        if user_id == '~':
            return current_user
        try:
            user = User.query.get(user_id)
            if not user:
                raise Exception('User doesn\'t find')
            resp = {
                'first_name': user.first_name,
                'last_name': user.last_name,
                'username': user.username,
                'email': user.email
            }
            return resp, 200
        except Exception as e:
            return e, 404

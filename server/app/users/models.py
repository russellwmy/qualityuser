from datetime import datetime
from app import app, db
import hashlib

class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.BigInteger, primary_key=True)
    first_name = db.Column(db.String)
    last_name = db.Column(db.String)
    name = db.Column(db.String)
    username = db.Column(db.String, unique=True)
    email = db.Column(db.String, unique=True)
    password = db.Column(db.String)

    status = db.Column(db.Integer, default=0)
    last_login_at = db.Column(db.DateTime, nullable=True)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.now)
    updated_at = db.Column(db.DateTime, nullable=False, default=datetime.now, onupdate=datetime.now)

    def get_fullname(self):
        return '{} {}'.format(self.first_name, self.last_name)

    def verify_password(self, password):
        import hashlib
        salt = app.config.get('SECRET_KEY')
        password = hashlib.sha512( (salt + password).encode('utf-8') ).hexdigest()
        return self.password == password

    def set_password(self, password):
        import hashlib
        salt = app.config.get('SECRET_KEY')
        self.password = hashlib.sha512( (salt + password).encode('utf-8') ).hexdigest()
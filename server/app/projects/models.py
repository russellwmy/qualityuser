from datetime import datetime
from app import db


class UserProject(db.Model):
    __tablename__ = 'user_projects'
    id = db.Column(db.BigInteger, primary_key=True)
    user_id = db.Column(db.BigInteger, db.ForeignKey('users.id'))
    project_id = db.Column(db.BigInteger, db.ForeignKey('projects.id'))

    created_at = db.Column(db.DateTime, nullable=False, default=datetime.now)
    updated_at = db.Column(db.DateTime, nullable=False, default=datetime.now, onupdate=datetime.now)

class Project(db.Model):
    __tablename__ = 'projects'

    id = db.Column(db.BigInteger, primary_key=True)
    name = db.Column(db.String)
    token = db.Column(db.String)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.now)
    updated_at = db.Column(db.DateTime, nullable=False, default=datetime.now, onupdate=datetime.now)
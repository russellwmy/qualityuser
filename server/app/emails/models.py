from datetime import datetime
from app import db
from app.users.models import User

class EmailStatus(db.Model):
    __tablename__ = 'email_statuses'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, nullable=True)
    recipient_email = db.Column(db.String)
    recipient_name = db.Column(db.String)
    code = db.Column(db.String)
    email_type = db.Column(db.Integer)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.now)
    updated_at = db.Column(db.DateTime, nullable=False, default=datetime.now, onupdate=datetime.now)
from flask import jsonify
from flask.ext.restful import request, Resource ,fields
from app.users.models import User
from app.emails.models import EmailStatus
from app import db, app
import jwt
import datetime


class UserRegistrationResource(Resource):

    def post(self):
        try:
            data = request.get_json(force=True)
            password = data.get('password', None)
            del data['password']
            user = User(**data)
            user.hash_password(password)
            db.session.add(user)
            db.session.commit()
            return 'accepted', 201
        except Exception as e:
            return e, 400
            
class LoginResource(Resource):
    
    def post(self):
        resp = {}
        try:
            data = request.get_json(force=True)
            email = data.get('email')
            password = data.get('password')
            user =user = User.query.filter(User.email==email).first()
            if user:
                if user.verify_password(password):
                    data = dict(
                            uid = user.id,
                            exp = datetime.datetime.now() + app.config.get('JWT_EXP_DELTA')
                    )
                    token = jwt.encode(data,app.config.get('SECRET_KEY'))
                    resp['access_token'] = token.decode('utf-8')
            else:
                raise Exception('User doesn\'t exist')
        except Exception as e:
            return e.message, 400
        return resp, 201


class PasswordResetResource(Resource):

    def get(self):
        try:
            email = request.args.get('email', None)
            user = User.query.filter_by(email=email).first()
            if user:
                email_status = EmailStatus()
                email_status.user_id = users.id
                email_status.recipient_name = user.get_fullname()
                email_status.recipient_email = user.email
                email_status.code = generate_code(10)
                email_status.email_type = EmailType.reset_password
                db.session.add(email_status)
                db.commit()
                return 'Accepted', 200
        except Exception as e:
                return e, 500
        return 'Account not found', 404

    def post(self):
        try:
            data = request.get_json(force=True)
            password = data.get('password', None)
            code = data.get('code', None)
            email_status = EmailStatus.query.filter_by(code=code, status=0).first()
            user =  User.query.get(email_status.user_id)
            user.hash_password(password)
            db.session.add(user)
            email_status.status = 1
            db.session.add(email_status)
            db.session.commit()
            return 'Accepted', 201
        except Exception as e:
            return e, 500

from app import db
from datetime import datetime
from sqlalchemy.dialects.postgresql import JSON


class ActionPixel(db.Model):
    
    __tablename__ = 'action_pixels'
    
    id = db.Column(db.BigInteger, primary_key=True)
    project_id = db.Column(db.BigInteger)
    token = db.Column(db.String, nullable=True)
    ip_address = db.Column(db.String, nullable=True)
    data = db.Column(JSON)

    created_at = db.Column(db.DateTime, nullable=False, default=datetime.now)
    updated_at = db.Column(db.DateTime, nullable=False, default=datetime.now, onupdate=datetime.now)

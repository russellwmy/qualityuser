from datetime import datetime
from flask.ext.restful import request, Resource, fields
from app import db
from app.actions.models import ActionPixel
from app.actions.utils import parse_data
from app.projects.models import Project



class ActionPixelTrackingResource(Resource):

    def post(self):
        try:

            action_pixel = ActionPixel()
            token = request.args.get('token', None)
            if not token:
                raise Exception('missing token')
            project = Project.query.filter(Project.token == token).first()
            if not project:
                raise Exception('project doesn\'t exists')
            action_pixel.project_id = project.id
            action_pixel.token = token
            action_pixel.ip_address = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            server_data = {}
            for k,v in request.headers:
                server_data[k] = v
            try:           
                client_data = request.get_json(force=True)
            except:
                client_data = {}
            action_pixel.data = parse_data(server_data, client_data)
            db.session.add(action_pixel)
            db.session.commit()
            return 'accepted', 200
        except Exception as e:
            return str(e), 400

    def get(self):
        try:
            action_pixel = ActionPixel()
            token = request.args.get('token', None)
            if not token:
                raise Exception('missing token')
            project = Project.query.filter(Project.token == token).first()
            if not project:
                raise Exception('project doesn\'t exists')
            action_pixel.project_id = project.id
            action_pixel.token = token
            action_pixel.ip_address = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            server_data = {}
            for k,v in request.headers:
                server_data[k] = v
            try:           
                client_data = request.args
            except:
                client_data = {}
            action_pixel.data = parse_data(server_data, client_data)
            db.session.add(action_pixel)
            db.session.commit()
            return 'accepted', 200
        except Exception as e:
            return str(e), 400

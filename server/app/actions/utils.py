import sys

if sys.version_info >= (3, 0):
    from urllib.parse import urlparse
if sys.version_info < (3, 0) and sys.version_info >= (2, 5):
    from urlparse import urlparse

from user_agents import parse


def parse_data(server_data, client_data):
    result = {}

    ua_string = server_data.get('User-Agent')
    user_agent = parse(ua_string)
    # Get user agent's browser attributes
    result['browser'] = user_agent.browser.family  # returns 'Mobile Safari'
    result['browser_version'] = user_agent.browser.version_string   # returns '5.1'
    # Get user agent's operating system properties
    result['operating_system'] = user_agent.os.family  # returns 'iOS'
    result['operating_system_version'] = user_agent.os.version_string  # returns '5.1'

    result['is_mobile'] = user_agent.is_mobile # returns False
    result['is_tablet'] = user_agent.is_tablet # returns False
    result['is_touch_capable'] = user_agent.is_touch_capable # returns True
    result['is_pc'] = user_agent.is_pc # returns True
    result['is_bot'] = user_agent.is_bot # returns False
    result['referrer'] = server_data.get('Referer', None)
    if result['referrer']:
        result['referrer_domain'] = urlparse(result['referrer']).netloc
    result['cookie'] = server_data.get('Cookie', None)
    accept_language = server_data.get('Accept-Language')
    if accept_language:
        accept_language = accept_language.split(';')[0]
        result['locale'] = accept_language.split(',')[0]
        result['language'] = accept_language.split(',')[1]
    for k, v in  client_data.items():
        result[k] = v
    return result


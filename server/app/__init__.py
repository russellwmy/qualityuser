import os
import sys
from flask import Flask
from flask.ext.restful import Api
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.cors import CORS

app = Flask(__name__)
cors = CORS(app)
app.config.from_object('config')
api = Api(app)
db = SQLAlchemy(app)


from app.users.models import *
from app.actions.models import *
from app.projects.models import *
db.create_all()


'''
User APIs routes
'''
API_URI = '/api'

from app.accounts.resources import UserRegistrationResource
api.add_resource(UserRegistrationResource,API_URI + '/register')

from app.accounts.resources import LoginResource
api.add_resource(LoginResource, API_URI + '/login')

from app.accounts.resources import PasswordResetResource
api.add_resource(PasswordResetResource, API_URI + '/password/reset')


from app.actions.resources import ActionPixelTrackingResource
api.add_resource(ActionPixelTrackingResource,API_URI + '/pixels')


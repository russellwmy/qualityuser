(function() {
    window._q = [];
    window._token = null;
    qu = {
        init: function(token){
            _token = token;
        },
        track: function(action, properties){
            
            _q.push({'action':action, 'properties':properties});
        }
    }
    window.qu = qu;
    function load_script(url)
    {
        var x = document.getElementsByTagName('script')[0];
        var s = document.createElement('script');
        s.async = true;
        s.src = url;
        x.parentNode.insertBefore(s, x);
        s.onreadystatechange = s.onload = function() {
            while(_q.length>0){
                item = _q.pop();
                _track(_token, item);
            }
        }
    }
    load_script('http://qualityuser.com/tracking/qu.js');
})();

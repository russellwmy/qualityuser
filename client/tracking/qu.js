(function(){
    var track_url = 'http://qualityuser.com/api/pixels';
    window._track = function (token,data){
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("POST", track_url+'?token='+token, true);
        xmlhttp.setRequestHeader("Content-type", "application/json");
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                console.log(xmlhttp.responseText);
            }
        }
        data = JSON.stringify(data);
        xmlhttp.send(data);
    }
})();

(function() {
    var track_url = 'http://qualityuser.com/api/track';
    var qa = {
        init: function (token){
            this.token = token;
        },
        track:function(action, properties){
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.open("POST", track_url+'?token='+this.token, true);
            xmlhttp.setRequestHeader("Content-type", "application/json");
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    console.log(xmlhttp.responseText);
                }
            }

            var parameters = {
                "action":action,
                "properties": properties
            };
            xmlhttp.send(parameters);    	 
        }
    };
    window.qa = qa;

})();